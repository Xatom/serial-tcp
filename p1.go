package main

import (
	"github.com/namsral/flag"
	"github.com/tarm/serial"
	"log"
)

const (
	BufferSize = 512

	Name     = "/dev/ttyUSB0"
	Baud     = 115200
	Size     = 8
	Parity   = 'N'
	StopBits = 1
)

var namePtr *string
var baudPtr *int
var sizePtr *int
var parityPtr *int
var stopBitsPtr *int

func DefineSerialFlags(fs *flag.FlagSet) {
	namePtr = fs.String("name", Name, "Name of the serial device")
	baudPtr = fs.Int("baud", Baud, "Baud rate of the serial device")
	sizePtr = fs.Int("size", Size, "Size of the serial device")
	parityPtr = fs.Int("parity", Parity, "Parity of the serial device")
	stopBitsPtr = fs.Int("stop-bits", StopBits, "Stop bits of the serial device")
}

func GetSerialConfig() *serial.Config {
	return &serial.Config{
		Name:     *namePtr,
		Baud:     *baudPtr,
		Size:     byte(*sizePtr),
		Parity:   serial.Parity(*parityPtr),
		StopBits: serial.StopBits(*stopBitsPtr),
	}
}

func StartSerial(config *serial.Config, inputChan chan []byte) {
	log.Printf("Starting serial using port %s with baud rate %d (%d%c%d)",
		config.Name, config.Baud, config.Size, config.Parity, config.StopBits)

	port, err := serial.OpenPort(config)
	if err != nil {
		log.Fatal(err)
	}

	readSerial(port, inputChan)
}

func readSerial(port *serial.Port, inputChan chan []byte) {
	for {
		buffer := make([]byte, BufferSize)

		count, err := port.Read(buffer)
		if err != nil {
			log.Fatal(err)
		}

		inputChan <- buffer[:count]
	}
}
