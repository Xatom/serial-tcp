package main

import (
	"github.com/namsral/flag"
	"log"
	"os"
)

func main() {
	printVersion()
	dataChan := make(chan []byte)

	fs := flag.NewFlagSetWithEnvPrefix(os.Args[0], "SERIAL_TCP", 0)
	DefineServerFlags(fs)
	DefineSerialFlags(fs)
	fs.Parse(os.Args[1:])

	serverConfig := GetServerConfig()
	serialConfig := GetSerialConfig()

	go StartServer(serverConfig, dataChan)
	StartSerial(serialConfig, dataChan)
}

func printVersion() {
	version := os.Getenv("SERIAL_TCP_VERSION")
	if version == "" {
		version = "Development"
	}

	log.Printf("SerialTcp @ %s", version)
}
