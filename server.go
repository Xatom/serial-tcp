package main

import (
	"fmt"
	"github.com/namsral/flag"
	"log"
	"net"
)

const (
	Host       = "localhost"
	Port       = 8888
	MaxClients = 25
)

type Config struct {
	Host       string
	Port       int
	MaxClients int
}

var hostPtr *string
var portPtr *int
var maxClientsPtr *int

func DefineServerFlags(fs *flag.FlagSet) {
	hostPtr = fs.String("host", Host, "Host of the server")
	portPtr = fs.Int("port", Port, "Port of the server")
	maxClientsPtr = fs.Int("max-clients", MaxClients, "Max clients of the server")
}

func GetServerConfig() *Config {
	return &Config{
		Host:       *hostPtr,
		Port:       *portPtr,
		MaxClients: *maxClientsPtr,
	}
}

func StartServer(config *Config, inputChan chan []byte) {
	log.Printf("Starting server on %s:%d with %d max clients",
		config.Host, config.Port, config.MaxClients)

	ln, err := net.Listen("tcp", fmt.Sprintf("%s:%d", config.Host, config.Port))
	if err != nil {
		log.Fatal(err)
	}

	connChan := make(chan net.Conn)
	go connWorker(config, connChan, inputChan)

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal(err)
		}

		connChan <- conn
	}
}

func connWorker(config *Config, connChan chan net.Conn, input chan []byte) {
	connections := make([]net.Conn, 0)

	for {
		select {
		case conn := <-connChan:
			if len(connections) < config.MaxClients {
				log.Printf("Client connected from %s", conn.RemoteAddr())
				connections = append(connections, conn)
			} else {
				log.Printf("Client refused from %s (max clients reached)", conn.RemoteAddr())
				conn.Close()
			}
		case data := <-input:
			for i := len(connections) - 1; i >= 0; i-- {
				_, err := connections[i].Write(data)
				if err != nil {
					log.Printf("Client disconnected from %s", connections[i].RemoteAddr())
					connections = append(connections[:i], connections[i+1:]...)
				}
			}
		}
	}
}
