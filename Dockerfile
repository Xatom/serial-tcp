FROM golang:alpine AS build

WORKDIR /go/src/serial-tcp
COPY . .

RUN apk add --no-cache git gcc musl-dev
RUN go get -v
RUN go build -v

FROM alpine:latest

WORKDIR /app
COPY --from=build /go/bin/serial-tcp .

ARG version=Test

ENV SERIAL_TCP_VERSION=${version}
ENV SERIAL_TCP_HOST=0.0.0.0

EXPOSE 8888

ENTRYPOINT ["./serial-tcp"]
