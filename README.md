# serial-tcp
Small utility to stream serial data over TCP.

# Usage
To start serial-tcp:

```bash
docker pull registry.gitlab.com/xatom/serial-tcp/$ARCH:latest
docker stop serial-tcp
docker rm serial-tcp
docker run -d --name serial-tcp --restart always \
    --device=$DEVICE \
    --network=bridge \
    registry.gitlab.com/xatom/serial-tcp/$ARCH:latest
```

Make sure to:
* Replace `$ARCH` with the CPU architecture of the host (`amd64` or `arm`)
* Replace `$DEVICE` with the name of the serial device (`/dev/ttyUSB0`)

# Configuration
The following options are available. You can add them to the `docker run` command by adding an `-e` flag:

```bash
docker run ...
    -e SOME_OPTION=SOME_VALUE \
```

* `SERIAL_TCP_NAME` (default: `/dev/ttyUSB0`)  
    Name of the serial device to read from  
    **Note:** also adjust the `--device` flag in `docker run`
* `SERIAL_TCP_BAUD` (default: `115200`)  
    Baud rate to use when reading from the serial device
* `SERIAL_TCP_SIZE` (default: `8`)  
    Number of data bits for the serial device
* `SERIAL_TCP_PARITY` (default: `N`)  
    Parity mode to use for the serial device
* `SERIAL_TCP_STOP_BITS` (default: `1`)  
    Stop bits to use for the serial device
* `SERIAL_TCP_HOST` (default: `localhost`)  
    Host to bind to the server  
    **Note:** shouldn't be changed for the Docker image
* `SERIAL_TCP_PORT` (default: `8888`)  
    Port to bind to the server
* `SERIAL_TCP_MAX_CLIENTS` (default: `25`)  
    Maximum number of clients allowed to connect concurrently
